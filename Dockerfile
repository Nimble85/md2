FROM ubuntu:16.04
MAINTAINER Serhii Klymoshenko "Serhii_Klymoshenko@epam.com.com"

# INSTALL WGET, JENKINS, JAVA, MAVEN, GIT
RUN apt update -y && apt install wget maven git curl -y && \
        wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | apt-key add - && \
        echo deb http://pkg.jenkins.io/debian-stable binary/ | tee /etc/apt/sources.list.d/jenkins.list && \
        apt update -y   && \
        apt install jenkins -y  && \
        apt install openjdk-8-jdk openjdk-8-jre -y

#INSTALL PLUGIN TO JENKINS
RUN apt update -y && \
        wget https://updates.jenkins-ci.org/download/war/2.121.2/jenkins.war && \
        mkdir -p ~/.jenkins/plugins && \
        cd ~/.jenkins/plugins && \
        wget https://repo.jenkins-ci.org/releases/org/jenkins-ci/plugins/job-dsl/1.33/job-dsl-1.33.jpi
        
#INSTALL NGINX  FOR LOAD BALANCE
#RUN apt update -y && apt install nginx -y
#COPY nginx.conf /etc/nginx/nginx.conf
#CMD systemctl reload nginx

VOLUME /var/lib/jenkins
EXPOSE 80
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "jenkins.war"]
